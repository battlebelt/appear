# Appear

## Use
To use, load the Appear script (es5 and es6 available) and the Appear stylesheet (css and scss available) in your application.

### Javascript part

`var appear = new Appear()` to create a new instance of the plugin. Only one is needed, no matter any element you want to animate.  
`appear.scrollUpdate()` have to be placed in a scroll event callback in order to trigger the animations (advice: use your scroll event with some bubbling or throttle system if you don't want to fire the callback too many times).  
**The plugin let you handle the scroll event by yourself**

### Template part (to be place in a html class attribute)
`js-appear-load` will trigger the animation on the loading of the page.  
**OR**  
`js-appear-scroll` will trigger the animation when the concerned element enters in the viewport.

#### Options (simply add them in the same html class attribute after the primary plugin class)

##### Position
`js-appear--from-top`  
`js-appear--from-right`  
`js-appear--from-bottom`  
`js-appear--from-left`  

##### Delay
`js-appear--delay-100` to `js-appear--delay-1000`

## Here's an exemple with throttling.

~~~
var appear = new Appear(),
    ticking = false;

/*
* scroll event handling with throttle
*/

function scrollUpdate() {
    if(!ticking) {
        window.requestAnimationFrame(function() {
            appear.scrollUpdate();
            ticking = true;
        });
    }
    ticking = false;
};

window.addEventListener('scroll', scrollUpdate, false);
~~~