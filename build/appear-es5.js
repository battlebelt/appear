'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Appear = function () {
    function Appear() {
        _classCallCheck(this, Appear);

        this.elementsScroll = document.querySelectorAll('.js-appear-scroll');
        this.elementsScrollLength = this.elementsScroll.length;
        this.scrollOffset = 200;

        this.elementsLoad = document.querySelectorAll('.js-appear-load');
        this.elementsLoadLength = this.elementsLoad.length;

        this.appearOnLoad();
        this.appearOnScroll();
        this.listenViewportHeight();
    }

    _createClass(Appear, [{
        key: 'appearOnLoad',
        value: function appearOnLoad() {
            var i = 0;
            for (i; i < this.elementsLoadLength; i++) {
                this.elementsLoad[i].classList.add('js-appear--active');
            }
        }
    }, {
        key: 'appearOnScroll',
        value: function appearOnScroll() {
            var i = 0;
            for (i; i < this.elementsScrollLength; i++) {
                if (this.elementsScroll[i].getBoundingClientRect().top < window.innerHeight) {
                    this.elementsScroll[i].classList.add('js-appear--active');
                } else {
                    this.elementsScroll[i].classList.remove('js-appear--active');
                }
            }
        }
    }, {
        key: 'scrollUpdate',
        value: function scrollUpdate() {
            this.appearOnScroll();
        }
    }, {
        key: 'listenViewportHeight',
        value: function listenViewportHeight() {
            window.onresize = function () {
                this.pageHeight = window.innerHeight;
            };
        }
    }]);

    return Appear;
}();