var gulp = require('gulp'),
    uglify = require('gulp-uglify'),
    plumber = require('gulp-plumber'),
    autoprefixer = require('gulp-autoprefixer'),
    concat = require ('gulp-concat'),
    sass = require('gulp-sass'),
    del = require('del'),
    babel = require('gulp-babel'),
    git = require('gulp-git'),
    bump = require('gulp-bump'),
    filter = require('gulp-filter'),
    tag_version = require('gulp-tag-version');

/**
 * Bumping version number and tagging the repository with it.
 * Please read http://semver.org/
 *
 * You can use the commands
 *
 *     gulp patch     # makes v0.1.0 → v0.1.1
 *     gulp minor   # makes v0.1.1 → v0.2.0
 *     gulp major   # makes v0.2.1 → v1.0.0
 *
 * To bump the version numbers accordingly after you did a patch,
 * introduced a feature or made a backwards-incompatible release.
 */
 
function inc(importance) {
    // get all the files to bump version in 
    return gulp.src(['./package.json', './bower.json'])
        // bump the version number in those files 
        .pipe(bump({type: importance}))
        // save it back to filesystem 
        .pipe(gulp.dest('./'))
        // commit the changed version number 
        .pipe(git.commit('bumps package version'))
 
        // read only one file to get the version number 
        .pipe(filter('package.json'))
        // **tag it in the repository** 
        .pipe(tag_version());
}
 
gulp.task('bump:patch', function() { return inc('patch'); })
gulp.task('bump:minor', function() { return inc('minor'); })
gulp.task('bump:major', function() { return inc('major'); })

gulp.task('scripts', function () {
    gulp.src('./src/js/**/*.js')
		.pipe(babel({
			presets: ['es2015']
		}))
        .pipe(concat('appear-es5.js'))
        .pipe(gulp.dest('./build'))
        .pipe(concat('appear-es5.min.js'))
        .pipe(uglify())
        .pipe(plumber())
		.pipe(gulp.dest('./build'));

    gulp.src('./src/js/**/*.js')
        .pipe(concat('appear-es6.js'))
        .pipe(plumber())
		.pipe(gulp.dest('./build'));
});

gulp.task('sass', function () {
    gulp.src('src/sass/**/*.scss')
        .pipe(plumber())
        .pipe(concat('appear.scss'))
        .pipe(gulp.dest('./build'));

    gulp.src('src/sass/**/*.scss')
        .pipe(plumber())
        .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
        .pipe(autoprefixer({
    		browsers: ['last 2 versions'],
    		cascade: false
    	}))
        .pipe(gulp.dest('./build'));
});

gulp.task('clean', function(cb) {
    return del('build/');
});

gulp.task('build', ['clean'],function() {
    gulp.start(['scripts', 'sass']);
});
gulp.task('default', function() {
    gulp.start('build');
});
