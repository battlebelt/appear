class Appear {
    constructor() {
        this.elementsScroll = document.querySelectorAll('.js-appear-scroll');
        this.elementsScrollLength = this.elementsScroll.length;
        this.scrollOffset = 200;

        this.elementsLoad = document.querySelectorAll('.js-appear-load');
        this.elementsLoadLength = this.elementsLoad.length;

        this.appearOnLoad();
        this.appearOnScroll();
        this.listenViewportHeight();
    }

    appearOnLoad() {
        let i = 0;
        for(i; i < this.elementsLoadLength; i++) {
            this.elementsLoad[i].classList.add('js-appear--active');
        }
    }

    appearOnScroll() {
        let i = 0;
        for(i; i < this.elementsScrollLength; i++) {
            if(this.elementsScroll[i].getBoundingClientRect().top < window.innerHeight) {
                this.elementsScroll[i].classList.add('js-appear--active');
            } else {
                this.elementsScroll[i].classList.remove('js-appear--active');
            }
        }
    }

    scrollUpdate() {
        this.appearOnScroll();
    }

    listenViewportHeight() {
        window.onresize = function() {
            this.pageHeight = window.innerHeight;
        };
    }
}
